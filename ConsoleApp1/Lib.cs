﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Lib
    {
        public int Id { get; set; }
        public int BooksCount { get; set; }
        public int SignUpProcess { get; set; }
        public int PerDay { get; set; }
        public List<Book> BookList { get; set; }
        public List<Book> BookNewList { get; set; }
        public int StartDay { get; set; }

        public Lib()
        {
            BookNewList = new List<Book>();
        }
    }
}
