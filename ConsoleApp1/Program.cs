﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        public static MainLib mainLib;
        public static List<Lib> libList = new List<Lib>();
        public static String filePart = "a_out.txt";
        public static String text = "";
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"D:\Desktop\hashcode\a_new.txt");
            //List<int> list = new List<int>();
            //for (int i = 1; i < lines.Length; i+=2)
            //{
            //    list.Add(int.Parse(lines[i].Split(" ")[0]));
            //    list.Sort();
            //}

            mainLib = new MainLib();
            mainLib.BooksCount = int.Parse(lines[0].Split(" ")[0]);
            mainLib.LibsCount = int.Parse(lines[0].Split(" ")[1]);
            mainLib.DaysCount = int.Parse(lines[0].Split(" ")[2]);
            var arr = lines[1].Split(" ");
            mainLib.BooksScores = StringArrToList(lines[1].Split(" "));
            int id = 0;
            for (int i = 2; i < lines.Length-1; i += 2)
            {
                var libProperties = lines[i].Split(" ");
                Lib lib = new Lib();
                lib.Id = id;
                lib.BooksCount = int.Parse(libProperties[0]);
                lib.SignUpProcess = int.Parse(libProperties[1]);
                lib.PerDay = int.Parse(libProperties[2]);
                var listLibsFromText = StringArrToListLib(lines[i + 1].Split(" "));
                lib.BookList = listLibsFromText;
                lib.StartDay = curProcess(libList.Count-1) + lib.SignUpProcess;
                libList.Add(lib);
                id++;
            }

            doMagic();

            writeResponse();
        }

        private static void doMagic()
        {
            bool isStop = false;
            for (int day = 0; day < mainLib.DaysCount; day++)
            {
                for (int k = 0; k < libList.Count; k++)
                {
                    if(libList[k].StartDay <= day || libList[k].BookNewList.Count != 0)
                    {
                        var count = libList[k].PerDay <= libList[k].BookList.Count ? libList[k].PerDay : libList[k].BookList.Count;
                        libList[k].BookNewList.AddRange(libList[k].BookList.GetRange(0, count));
                        libList[k].BookList.RemoveRange(0, count);
                    }
                }

                if (isStop) break;
            }
        }


        private static int curProcess(int libId) {
            int cur = 0;
            for (int i = 0; i < libId; i++)
            {
                cur += libList[i].SignUpProcess;
            }
            return cur;
        }

        private static void writeResponse()
        {
            String text = "";
            int count = 0;
            for (int i = 0; i < libList.Count; i++)
            {
                if (libList[i].BookNewList.Count == 0) continue;
                text += libList[i].Id + " " + libList[i].BookNewList.Count + "\n";
                var result = string.Join(" ", libList[i].BookNewList.Select(s=> s.Id).ToList<int>());
                text += result + "\n";
                count++;
            }
            text = count + "\n" + text ;
            System.IO.File.WriteAllText(@"D:\Desktop\hashcode\"+ filePart, text);
        }

        public static List<int> StringArrToList(String[] arr)
        {
            List<int> integerList = new List<int>();
            foreach (var item in arr)
            {
                integerList.Add(int.Parse(item));
            }
            return integerList;
        }

        public static List<Book> StringArrToListLib(String[] arr)
        {
            List<Book> bookList = new List<Book>();
            for (int i = 0; i < arr.Length; i++)
            {
                Book book = new Book();
                book.Id = int.Parse(arr[i]);
                book.LibId = i;
                bookList.Add(book);
            }
            return bookList;
        }
    }
}
