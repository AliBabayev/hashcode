﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class MainLib
    {
        public int BooksCount { get; set; }
        public int LibsCount { get; set; }
        public int DaysCount { get; set; }
        public List<int> BooksScores { get; set; }
    }
}
